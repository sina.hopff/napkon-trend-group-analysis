#load packages
pacman::p_load(here, sjmisc, car, gmodels, vcd, haven, rio, naniar, MatchIt, MatchThem, cobalt, readr, lubridate, ggplot2, table1, mice, labelled, dplyr, tidyverse, tableone, mitools, moments, psych)

#Read and start preprocessing data
source("R/01_read_data_NAPKON-Trend-Group-Analysis.R", encoding = 'UTF-8')
source("R/02_load_functions_for_preprocessing_NAPKON-Trend-Group_Analysis.R", encoding = 'UTF-8')

# Preprocessing and Selection of the cohort
data_primary_coded$bv1 <- unify_ethnicity_with_other(data_primary_coded$bv1, data_primary_coded$export_options)

#VOC variants
#first documented voc variant is now in fv9 (Parent table of epcr) for each patient in new var voc_group
data_primary_coded$epcr <- unify_voc_with_other(data_primary_coded$epcr, data_primary_coded$export_options)
data_primary_coded$fv9 <- compute_voc_variants(data_primary_coded$fv9, data_primary_coded$epcr, data_primary_coded$bv2)

# Vaccination info/details  (number of, name and days of vac) -> is in data_primary_coded$fv7
data_primary_coded$fv7 <- compute_vac_details(data_primary_coded$fv7, data_primary_coded$evac_sars, data_primary_coded$bv2)

#Comorbidities
#identify patients with any diagnosis
data_primary_coded$voc_comorbidity <- build_any_precovid_comorbidity_df(data_primary_coded)

#process patients with detailed diagnosis
source("R/03_define_comorbidities_NAPKON-Trend-Group-Analysis.R", encoding = 'UTF-8')

#Symptoms
# data_primary_coded$voc_sy_parents_and_children <- build_sy_parents_children_merged_df(data_primary_coded)
# 
# any_sy_by_family <- data_primary_coded$voc_sy_parents_and_children %>%
#   group_by(export_psn, sy_family, formname) %>% #, formname, formtablename, subformtablename, fglabel, sy_extracted_name) %>%
#   summarise(any_sy_parent = 
#               case_when(any(sy_parent=="Ja") ~ "yes",
#                         all(sy_parent=="Nein") ~ "no",
#                         any(sy_parent == "Keine Informationen verfügbar") ~ "no info")) %>%
#   ungroup()
# 
# #26.05.23 jedes symptom jemals als spalte pro Pat. eine Zeile
# every_sy_by_pat <- data_primary_coded$voc_sy_parents_and_children %>%
#   group_by(export_psn, sy_extracted_name_parent) %>% #, formname, formtablename, subformtablename, fglabel, sy_extracted_name) %>%
#   summarise(sy_is_present = 
#               case_when(any(sy_parent=="Ja") ~ "yes",
#                         all(sy_parent=="Nein") ~ "no",
#                         any(sy_parent == "Keine Informationen verfügbar") ~ "no info")) %>%
#   ungroup() %>%
#   pivot_wider(id_cols = export_psn, names_from = sy_extracted_name_parent, values_from = sy_is_present) 
#
# sy_var_labs <- data_primary_coded$is %>%
#   filter(ffcolname %in% names(every_sy_by_pat)) %>%
#   #group_by(ffcolname)%>%
#   #slice_min(fgid, with_ties = FALSE)
#   select(fflabel, ffcolname) %>%
#   distinct(ffcolname, .keep_all = TRUE)
# 
# # hier mit Symptom Labels
# every_sy_by_pat <- every_sy_by_pat %>%
#   imodify(~ .x %>% sjlabelled::set_label(sy_var_labs$fflabel[sy_var_labs$ffcolname==.y])) %>%
#   select(export_psn, any_of(sy_var_labs$ffcolname), everything())

#WHO Clinical Progression Scale
data_primary_coded$voc_who_scale_df <- build_who_scale_df(data_primary_coded)

#select and modify variables for analysis
source("R/04_Prepare_data_for_analysis_NAPKON-Trend-Group-Analysis.R", encoding = 'UTF-8')

#write.csv(data_analysis, file = "Mastertabelle_NAPKON-Trend-Group-Analysis_2024-04-22.csv")

